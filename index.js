var app = require('express')();
var http = require('http').createServer(app);
module.exports = app;
// var io = require('socket.io')(http);
const io = require("socket.io")(http, {
	cors: {
	  origin: "*",
	  methods: ["GET", "POST"]
	}
  });

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
/*
io.on('connection', (socket) => {
  io.emit('user_connect')
  console.log('user_connect'.socket);
  socket.on('disconnect', () => {
    console.log('user disconnected');
	io.emit('user_disconnect')
  });
});
/*/
let numUsers = 0;

// io.sockets.on('connection', function(socket) {
	
// 	  socket.on('create', function(room) {
// 		console.log('trigger1'+room)
// 		socket.join(room);
// 	  });
// 	});
var roomno = 1;

io.sockets.on('connection', function (socket) {
	
  });

io.on('connection', (socket) => {
	
	socket.on('join', function (roomnumber) {
		roomno=roomnumber;
		socket.roomnumbers=roomnumber;
		console.log('set roomno',roomnumber)
	    socket.join(roomnumber); // We are using room of socket io
	});

	let addedUser = false;
	  
		  //socket.on('chat message', (msg) => {
			//io.emit('chat message', msg);
		  //});
	socket.on('new message', (data) => {
		// let message = data.data;
		// let room = data.room;
		// console.log('roomno : '+roomno)
		// we tell the client to execute 'new message'
		io.sockets.in(socket.roomnumbers).emit('new message', {
			username: socket.username,
			message: data
		});
		// socket.to(123).emit('event');
		// io.emit('new_msg', 'hello');
		// io.emit('new message', {
		//   username: socket.username,
		//   message: data
		// });
	});

	
	//    socket.emit('getCount',total)
	   
	socket.on('getCount', () => {
		var total=socket.username;
		// io.sockets.in(socket.roomnumbers).emit('getCount', total);
		socket.emit('getCount',total)
	});
	
	  // when the client emits 'add user', this listens and executes
	socket.on('add user', (username) => {
		if (addedUser) return;
		// console.log('user set',username);
		// we store the username in the socket session for this client
		socket.username = username;
		++numUsers;
		addedUser = true;
		socket.emit('login', {
			numUsers: numUsers
		});
		// echo globally (all clients) that a person has connected
		socket.broadcast.in(socket.roomnumbers).emit('user joined', {
			username: socket.username,
			numUsers: numUsers
		});
	});
	 
	socket.on('disconnect', () => {
		// console.log('disconnect',addedUser)
		if (addedUser) {
		  --numUsers;
			// console.log('disconnect',socket.username)
		  // echo globally that this client has left
		  socket.broadcast.in(socket.roomnumbers).emit('user left', {
			username: socket.username,
			numUsers: numUsers
		  });
		}
	  });
  
});



http.listen(3000, () => {
  console.log('listening on *:3000');
});